#!/usr/bin/env bash

set -euo pipefail
set -x

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_dir"
version=$1; shift
out_dir=$1; shift

checkout()
{
    if [ -d jenkins ]; then
        return
    fi

    git clone https://github.com/jenkinsci/jenkins/
    pushd jenkins
    git checkout $version
    git log -n1
    # maven complains about some xml files:
    cmd /c "mvn spotless:apply"
    popd
}

get_maven()
{
    export PATH=$(pwd)/maven/bin:$PATH
    if [ -d maven ]; then
        return
    fi
    wget https://dlcdn.apache.org/maven/maven-3/3.9.6/binaries/apache-maven-3.9.6-bin.tar.gz
    tar xvf apache-maven-*.tar.gz
    rm *.tar.gz
    mv apache-maven-* maven
}

get_node()
{
    # by default, Jenkins download node for i386, which will fail to compile
    # WebAssembly modules because of RAM limitation. Thus, replace manually with
    # native arm64.
    # "failed to compile wasm module: RangeError: WebAssembly.Instance(): Out of memory: Cannot allocate Wasm memory for new instance"

    if [ ! -f jenkins/war/node/node.exe ]; then
        mkdir -p jenkins/war/node/
        wget https://nodejs.org/dist/v20.11.1/win-arm64/node.exe \
            -O jenkins/war/node/node.exe
    fi
}

build()
{
    # https://github.com/jenkinsci/jenkins/blob/master/CONTRIBUTING.md#building-and-debugging
    pushd jenkins
    cmd /c "mvn -am -pl war,bom -Pquick-build clean install"
    # mvn keeps cache (download, build) in $USERPROFILE/.m2 folder
    file ./war/target/jenkins.war
    # run with: java -jar ./war/target/jenkins.war
    popd
}

list_native_binaries()
{
    rm -rf unpack
    mkdir -p unpack
    cp -r jenkins/war/target/jenkins.war unpack/
    pushd unpack

    # fully unpack .war/.jar files
    unzip -q jenkins.war
    for jar in $(find | grep '\.jar$'); do
        echo $jar
        name=$(basename $jar).content
        mkdir -p $name
        pushd $name
        unzip -q ../$jar
        popd
    done

    # now list all .dll and .exe
    find | grep -E '\.(exe|dll)$' | xargs -n1 file
    popd
}

get_maven
checkout
get_node
build
list_native_binaries